# https://www.statmethods.net/graphs/index.html

#>> Creating a Graph <<#
# https://www.statmethods.net/graphs/creating.html

#print(mtcars) # print built-in dataset
#attach(mtcars) # so won't have to type mtcars$mpg
#print(typeof(mtcars))

# linear regression
# l$coefficients etc
l = lm(mtcars$mpg ~ mtcars$wt)



#png("mygraph.png") # save to file

plot(mtcars$wt, mtcars$mpg)
abline(l)
title("Regression of MPG on Weight")

#dev.off() # flush to file?

hist(mtcars$mpg)
