# R Development #
Setup R development environment.

## Install R
Download `R` base distribution from [CRAN Mirrors](https://cran.r-project.org/mirrors.html).  Install `R`.

Download `RStudio` from [RStudio](https://rstudio.com/products/rstudio/download/#download). Install.


## Install Packages ##
Some basic package commands:

* Install from CRAN with `install.package("x")`.
* Load the library with `library("x")`.
* Get help with `package?x` and `help(package = "x")`

Install basic R packages to facilitate the development process
```R
install.packages(c("devtools", "roxygen2", "testthat", "knitr"))
```
See [R packages](http://r-pkgs.had.co.nz/intro.html) for more details.

## Creating a Package ##
See [R packages](http://r-pkgs.had.co.nz/package.html) for creating packages in
RStudio or using command line as following
```R
devtools::create("path/to/package/pkgname")
```
For example
```R
devtools::create("IntroR")
```
Use RStudio to load a package project or using command line as following
Use
```R
getwd()
setwd("your/package/directory")
```
Load development tool and load package under developing
```R
library(devtools)
load_all() # Load package
test() # Test package
```
Reload the package in RStudio with `ctrl-shift-l`.

## Testing ##
Set up testing directory for `testthat`
```R
# Change working directory
setwd("your/package/dir")
devtools::use_testthat()
```
Create testing R script in `your/package/dir/tests/testthat`.  The file name must
start with `test`.

For example create a file named `testCh3.R` in `.../tests/testthat`
```R
test_that("length",{
  x <- 1:10
  expect_equal(length(x), 10)
  # - shorten the length - #
  length(x) <- 1
  expect_equal(length(x), 1)
})
```
Run the test in RStudio with `ctrl-shift-t` or `devtools::test()` from CLI.

