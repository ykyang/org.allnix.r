printf <- function(...) cat(sprintf(...))

age <- c(1,3,5,2,11,9,3,9,12,3)
print(age)
weight <- c(4.4,5.3,7.2,5.2,8.5,7.3,6.0,10.4,10.2,6.1)
print(weight)

printf("mean = %f\n", mean(weight))
printf("sd = %f\n", sd(weight))
printf("cor = %f\n", cor(age,weight))

plot(age,weight)

options(digits = 3)
x <- runif(20)
printf("runif: ")
print(x)
print(summary(x))
hist(x)
