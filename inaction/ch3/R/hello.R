# Hello, world!
#
# This is an example function named 'hello'
# which prints 'Hello, world!'.
#
# You can learn more about package authoring with RStudio at:
#
#   http://r-pkgs.had.co.nz/
#
# Some useful keyboard shortcuts for package authoring:
#
#   Build and Reload Package:  'Ctrl + Shift + B'
#   Check Package:             'Ctrl + Shift + E'
#   Test Package:              'Ctrl + Shift + T'

printf <- function(...) cat(sprintf(...))

hello <- function() {
  print("Hello, world!")
}

# - 3.1 - #

with(mtcars,{
# uncomment to print to PDF
#  pdf("mygraph.pdf")
  plot(wt, mpg)
  abline(lm(mpg~wt))
  title("Regression of MPG on Weight")
#  dev.off()
})
#print(mtcars)

# - 3.2 A simple example - #
dose  <- c(20, 30, 40, 45, 60)
drugA <- c(16, 20, 27, 40, 60)
drugB <- c(15, 18, 25, 31, 40)

plot(dose, drugA, type='b')
title("Just plot")

# customization
opar <- par(no.readonly = TRUE)
par(lty=2, pch=17)
plot(dose, drugA, type='b')
title("Using par()")
par(opar)

# same as above customization
plot(dose, drugA, type='b', lty=2, pch=17)
title("Direct")

# Uncomment to clear Plots window
# graphics.off()
